> > > # Bachelor Thesis Crypto Tracker

Django Implementation von plattformübergreifenden Zugang zu Daten verschiedener Kryptobörsen

to be able to run the code please make sure you fullfill the following steps: 

1. Please install the following dependencies to be able to run the code:

 
- `pip install Django`
 
- `python -m pip install -U channels`

-  `pip install python-binance`

-  `pip install ccxt`


2. To start the code, use the following command:
`python manage.py runserver`

3. In the file config.py fill in your personal Credentials:


> API_KEY = 'Your Binance API Key'

> API_SECRET = 'Your Binance API Secret Key'



> COINBASEPRO_API_KEY = 'Your Coinbase Pro API Key'

> COINBASEPRO_API_SECRET = 'Your Coinbase Pro API Key'

> COINBASEPRO_PASSWORD = 'Your Coinbase Pro API Password'

