import config
from binance.client import Client


client = Client(config.API_KEY, config.API_SECRET)


info = client.get_account()
