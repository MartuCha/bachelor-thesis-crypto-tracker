from django.urls import path
from boards import views

urlpatterns = [
    path('buy/', views.buy, name='buy'),
    path('sell/', views.sell, name='sell'),
    path('buycb/', views.buycb, name='buycb'),
    path('sellcb/', views.sellcb, name='sellcb'),
    path('', views.main, name='home')
]