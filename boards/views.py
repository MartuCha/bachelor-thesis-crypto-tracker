from binance.enums import *
from binance.exceptions import BinanceAPIException
from django.http import HttpResponse
from django.shortcuts import render, redirect
from .services import client
from .form import OrderForm
from binance.enums import SIDE_BUY, ORDER_TYPE_MARKET
import ccxt
import config


def account_info():
	account = client.get_account()
	balances = account['balances']
	exchange_info = client.get_exchange_info()
	symbols = exchange_info['symbols']

	form = OrderForm()
	return balances, symbols, form



def main(request):
	balances, symbols, form = account_info()
	balancescb = cbaccount_info()
	return render(
		request, 'main.html', context={'my_balancescb': balancescb, 'text': 'binance', 'my_balances': balances, 'symbols': symbols, 'form': form}
	)

exchange = ccxt.coinbasepro({
    'apiKey': config.COINBASEPRO_API_KEY,
    'secret': config.COINBASEPRO_API_SECRET,
    'password' : config.COINBASEPRO_PASSWORD

})

def cbaccount_info():
	coinbaseProAccountInfo = exchange.fetch_balance()
	balancescb = coinbaseProAccountInfo['info']

	return balancescb


def buy(request):
	balances, symbols, form = account_info()
	# if this is a POST request we need to process the form data
	if request.method == 'POST':
#?WTF	# create a form instance and populate it with data from the request:
		form = OrderForm(request.POST)
		# check whether it's valid:
		if form.is_valid():
			# process the data in form.cleaned_data as required
			# ...
			# redirect to a new URL:
			try:
				order = client.create_order(
					symbol=form.cleaned_data['symbol'], #reach the data like a dic; cleaned_data: clean up the data for you unencripted. ['symbol'] - name of the field we're getting
					side=SIDE_BUY,
					type=ORDER_TYPE_MARKET,
					quantity=form.cleaned_data['quantity']
				)
			except BinanceAPIException as e:
				return render(
					request, 'main.html',
					context={'text': 'binance', 'my_balances': balances, 'symbols': symbols, 'form': form, 'error': f'{e}'}
				)
	return redirect('/')

def sell(request):
	balances, symbols, form = account_info()
	# if this is a POST request we need to process the form data
	if request.method == 'POST':
	# create a form instance and populate it with data from the request:
		form = OrderForm(request.POST)
		# check whether it's valid:
		if form.is_valid():
			# process the data in form.cleaned_data as required
			# ...
			# redirect to a new URL:
			try:
				order = client.create_order(
					symbol=form.cleaned_data['Market'], #reach the data like a dic; cleaned_data: clean up the data for you unencripted. ['symbol'] - name of the field we're getting
					side=SIDE_SELL,
					type=ORDER_TYPE_MARKET,
					quantity=form.cleaned_data['quantity']
				)
			except BinanceAPIException as e:
				return render(
					request, 'main.html',
					context={'text': 'binance', 'my_balances': balances, 'symbols': symbols, 'form': form, 'error': f'{e}'}
				)
	return redirect('/')

def buycb(request):
	balances, symbols, form = account_info()
	# if this is a POST request we need to process the form data
	if request.method == 'POST':
	# create a form instance and populate it with data from the request:
		form = OrderForm(request.POST)
		# check whether it's valid:
		if form.is_valid():
			# process the data in form.cleaned_data as required
			# ...
			# redirect to a new URL:
			try:
				order = exchange.create_market_buy_order(form.cleaned_data['Market'], int(form.cleaned_data['quantity']))

			except BinanceAPIException as e:
				return render(
					request, 'main.html',
					context={'my_balances': balances, 'symbols': symbols, 'form': form, 'error': f'{e}'}
				)
	return redirect('/')

def sellcb(request):
	balances, symbols, form = account_info()
	# if this is a POST request we need to process the form data
	if request.method == 'POST':
	# create a form instance and populate it with data from the request:
		form = OrderForm(request.POST)
		# check whether it's valid:
		if form.is_valid():
			# process the data in form.cleaned_data as required
			# ...
			# redirect to a new URL:
			try:
				order = exchange.create_market_sell_order(form.cleaned_data['Market'], int(form.cleaned_data['quantity']))

			except BinanceAPIException as e:
				return render(
					request, 'main.html',
					context={'my_balances': balances, 'symbols': symbols, 'form': form, 'error': f'{e}'}
				)
	return redirect('/')




