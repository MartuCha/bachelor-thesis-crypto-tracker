

let ws = new WebSocket('wss://stream.binance.com:9443/stream?streams=btcusdt@trade/ethusdt@trade/adausdt@trade/dogeusdt@trade/dogeusdt@trade/bchusdt@trade/ltcusdt@trade/atomusdt@trade');
        let btc = document.getElementById('btc');
        let eph = document.getElementById('eth');
        let ada = document.getElementById('ada');
        let doge = document.getElementById('doge');
        let bch = document.getElementById('bch');
        let ltc = document.getElementById('ltc');
        let atom = document.getElementById('atom');



        let binlastPrice = null;

        function binchange_price(stockObject, coin) {
            let price = parseFloat(stockObject.data.p).toFixed(3);
            coin.innerText = price;
            coin.style.color = !binlastPrice || binlastPrice === price ? 'black' : price > binlastPrice ? 'green' : 'red';
            binlastPrice = price;
        }

        ws.onmessage = (event) => {
            let stockObject = JSON.parse(event.data);
            //console.log(stockObject);
            if (stockObject.stream === "btcusdt@trade") {
                binchange_price(stockObject, btc)
            }
            if (stockObject.stream === "ethusdt@trade") {
                binchange_price(stockObject, eph)
            }
            if (stockObject.stream === "adausdt@trade") {
                binchange_price(stockObject, ada)
            }
            if (stockObject.stream === "dogeusdt@trade") {
                binchange_price(stockObject, doge)
            }
            if (stockObject.stream === "bchusdt@trade") {
                binchange_price(stockObject, bch)
            }
            if (stockObject.stream === "ltcusdt@trade") {
                binchange_price(stockObject, ltc)
            }
            if (stockObject.stream === "atomusdt@trade") {
                binchange_price(stockObject, atom)
            }

        }

function close() {
    ws.close()
}

let socket = new WebSocket("wss://ws-feed.pro.coinbase.com");

          let cbtc = document.getElementById('cbtc');
          let ceth = document.getElementById('ceth');
          let cada = document.getElementById('cada');
          let cdoge = document.getElementById('cdoge');
          let cbch = document.getElementById('cbch');
          let cltc = document.getElementById('cltc');
          let catom = document.getElementById('catom');

          let message = {
                'type':'subscribe',
                'channels':[
                    {'name':'ticker',
                    'product_ids':['BTC-USD', 'ETH-USD', 'ADA-USD', 'DOGE-USD', 'BCH-USD', 'LTC-USD', 'ATOM-USD']}
                ]
        }

          socket.onopen = function (e) {
            alert("Sending to server");
            socket.send(JSON.stringify(message))
          };

          let cblastPrice = null;

          function cbchange_price(stockObject, coin) {
            let price = parseFloat(stockObject.price).toFixed(3);
            coin.innerText = price;

        }

          socket.onmessage = function (event) {
            let stockObject = JSON.parse(event.data);

            if (stockObject.product_id === 'BTC-USD'){
                cbchange_price(stockObject, cbtc)
            }

            if (stockObject.product_id === 'ETH-USD'){
                cbchange_price(stockObject, ceth)
            }
            if (stockObject.product_id === 'ADA-USD'){
                cbchange_price(stockObject, cada)
            }
            if (stockObject.product_id === 'DOGE-USD'){
                cbchange_price(stockObject, cdoge)
            }
            if (stockObject.product_id === 'BCH-USD'){
                cbchange_price(stockObject, cbch)
            }
            if (stockObject.product_id === 'LTC-USD'){
                cbchange_price(stockObject, cltc)
            }
            if (stockObject.product_id === 'ATOM-USD'){
                cbchange_price(stockObject, catom)
            }


          }

          function close() {
            socket.close()
        }